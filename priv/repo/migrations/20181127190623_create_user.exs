defmodule Takso.Repo.Migrations.CreateUser do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :name, :string
      add :password, :string
      add :phone_number, :string

      timestamps()
    end

    create unique_index(:users, [:phone_number])
  end
end
