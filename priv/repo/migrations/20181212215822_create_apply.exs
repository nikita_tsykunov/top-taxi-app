defmodule Takso.Repo.Migrations.CreateApply do
  use Ecto.Migration

  def change do
    create table(:apply) do
      add :user_id, references(:users, on_delete: :nothing)
      add :name, :string
      add :lastName, :string
      add :email, :string
      add :dateOfBirth, :date
      add :licenseNum, :string
      add :licenseCat, :string
      add :additionalInfo, :string
      add :status, :string

      timestamps()
    end
  end
end
