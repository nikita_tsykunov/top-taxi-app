defmodule Takso.Repo.Migrations.AdditionalTaxiInfo do
  use Ecto.Migration

  def change do
    create table(:taxi_info, primary_key: false) do
      add :driver_id, references(:users, on_delete: :nothing), primary_key: true
      add :lat, :float, default: 0.0
      add :lng, :float, default: 0.0
      add :rating_score, :float, default: 0.0
      add :rating_qty, :integer, default: 0
      add :car, :string
      add :additional_info, :string
      add :online, :boolean, default: false
    end

    create unique_index(:taxi_info, [:driver_id])
  end
end
