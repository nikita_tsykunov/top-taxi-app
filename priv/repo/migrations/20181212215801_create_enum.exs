defmodule Takso.Repo.Migrations.CreateEnum do
  use Ecto.Migration

  def up do
    ApplicationStatusEnum.create_type()
  end

  def down do
    ApplicationStatusEnum.drop_type()
  end
end
