defmodule Takso.Repo.Migrations.CreateOrder do
  use Ecto.Migration

  def change do
    create table(:orders) do
      add :pickup_address, :string
      add :dropoff_address, :string
      add :pickup_lat, :float
      add :pickup_lng, :float
      add :dropoff_lat, :float
      add :dropoff_lng, :float
      add :status, :string
      add :declines, :integer, default: 0
      add :driver_id, references(:users, on_delete: :nothing)
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:orders, [:user_id])
  end
end
