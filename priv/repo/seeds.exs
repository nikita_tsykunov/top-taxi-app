alias Takso.{Repo, User, Role, Order}

changeset = User.changeset(%User{}, %{name: "Ada",password: "password", phone_number: "+372 1111 1111"})
user = Repo.insert!(changeset) |> Repo.preload(:roles)

[%{role: "Operator"}, %{role: "Driver"}, %{role: "User"}]
|> Enum.map(fn role_data -> Role.changeset(%Role{}, role_data) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)

role = Repo.get_by(Role, role: "Operator") |> Repo.preload(:users)

user
|> Ecto.Changeset.change
|> Ecto.Changeset.put_assoc(:roles, [role])
|> Repo.update

# It's temporary
# TODO: Delete later
[%{pickup_address: "A",dropoff_address: "B", status: "active", user_id: 1, driver_id: 2}, %{pickup_address: "C",dropoff_address: "D", status: "active", user_id: 1, driver_id: 2}, %{pickup_address: "X",dropoff_address: "Y", status: "canceled", user_id: 1, driver_id: 2}]
|> Enum.map(fn order_data -> Order.changeset(%Order{}, order_data) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)

changeset = User.changeset(%User{}, %{name: "Artur",password: "password", phone_number: "+372 2222 2222"})
user = Repo.insert!(changeset) |> Repo.preload(:roles)
role = Repo.get_by(Role, role: "Driver") |> Repo.preload(:users)

user
|> Ecto.Changeset.change
|> Ecto.Changeset.put_assoc(:roles, [role])
|> Repo.update
