// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".

import "phoenix_html";
import "bootstrap";
import "bootstrap-select";
// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

// import socket from "./socket"

import Vue from "vue";
import axios from "axios";

new Vue({
  el: '#taxi-app',
  data: {
    map: null,
    user_id: -1,
    pickupMarker: null,
    pickup_address: "",
    pickup_coords: null,
    dropoff_address: "",
    dropoff_coords: null,
    dropOffMarker: null,
    bounds: null,
    directionsDisplay: null,
    directionsService: null
  },
  methods: {
    deleteDropoff: function() {
        if (this.dropOffMarker != null) {
          this.dropOffMarker.setMap(null);
          this.dropOffMarker = null;
        }
    },
  
    setDropoff: function(position) {
        this.deleteDropoff();
        this.dropoff_coords = {lat: position.lat(), lng: position.lng()};
        var self = this;
        this.dropOffMarker = new google.maps.Marker({
          position: position,
          map: self.map
        });
        this.dropOffMarker.setLabel('B');
        this.geocoder.geocode({ location: position }, (results, status) => {
          if (status === "OK" && results[0])
            self.dropoff_address = results[0].formatted_address;
        });
        var tempBounds = self.bounds;
        tempBounds.extend(position);
        self.map.fitBounds(tempBounds);
    },

    initMap: function() {
        var self = this;
        var autocomplete;
        this.bounds = new google.maps.LatLngBounds();
        this.directionsDisplay = new google.maps.DirectionsRenderer();
        this.directionsService = new google.maps.DirectionsService();

        navigator.geolocation.getCurrentPosition(position => {
          let loc = {lat: position.coords.latitude, lng: position.coords.longitude};
          self.pickup_coords = loc;
          this.geocoder = new google.maps.Geocoder;
          this.geocoder.geocode({location: loc}, (results, status) => {
              if (status === "OK" && results[0])
                self.pickup_address = results[0].formatted_address;
            });
          self.map = new google.maps.Map(document.getElementById('map'), {zoom: 14, center: loc, disableDefaultUI: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP});
          self.map.addListener('click', function(event) {
            self.setDropoff(event.latLng);
          });

          autocomplete = new google.maps.places.Autocomplete(document.getElementById('dropoff_address'));
          autocomplete.bindTo('bounds', self.map);
          autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);

          autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }
  
            // If the place has a geometry, then present it on a map.
            self.setDropoff(place.geometry.location);
          });

          self.pickupMarker = new google.maps.Marker({position: loc, map: self.map, title: "Pickup address"});
          self.pickupMarker.setLabel('A');
          self.bounds.extend(loc);

          self.directionsDisplay.setMap(self.map);
        });
    },

    submitBookingRequest: function() {
        this.user_id = document.getElementById('user_id').value;
        var self = this;
        console.log(this.dropoff_coords);
        axios.request({url: "/client/orders",
        method: "post",
        data: {
            user_id: this.user_id,
            pickup_lat: this.pickup_coords.lat,
            pickup_lng: this.pickup_coords.lng,
            pickup_address: this.pickup_address,
            dropoff_lat: this.dropoff_coords.lat,
            dropoff_lng: this.dropoff_coords.lng,
            dropoff_address: this.dropoff_address,
        },
        withCredentials: true
        });
        var request = {
            origin: this.pickup_coords,
            destination: this.dropoff_coords,
            travelMode: google.maps.TravelMode.DRIVING
          };
          self.directionsService.route(request, function(response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                console.log(response);
                self.directionsDisplay.setDirections(response);
                var leg = response.routes[0].legs[0]
                document.getElementById('distance').innerText = leg.distance.text;
                document.getElementById('time').innerText = leg.duration.text;
                document.getElementById('price').innerText = (2.80 + leg.distance.value * 0.00065).toFixed(2) + " �";
                self.pickupMarker.setVisible(false);
                self.deleteDropoff();
            } else {
              alert("Directions Request failed: " + status);
            }
          });
          document.getElementById('taxi-app').classList.add('d-none');
          document.getElementById('info').classList.remove('d-none');
          google.maps.event.clearListeners(self.map, 'click');
    },
  
    geocodeAddress: function() {
        var geocoder = new google.maps.Geocoder;
        var self = this;
        geocoder.geocode({'address': this.dropoff_address}, function(results, status) {
          if (status === 'OK') {
            self.deleteDropoff();
            self.map.setCenter(results[0].geometry.location);
            self.dropOffMarker = new google.maps.Marker({
              map: self.map,
              position: results[0].geometry.location
            });
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
    }
  },

  mounted: function () {
      this.initMap();
  }
});