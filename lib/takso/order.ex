defmodule Takso.Order do
  use Ecto.Schema
  import Ecto.Changeset

  schema "orders" do
    field :pickup_address, :string
    field :dropoff_address, :string
    field :pickup_lat, :float
    field :pickup_lng, :float
    field :dropoff_lat, :float
    field :dropoff_lng, :float
    field :status, :string
    field :declines, :integer, default: 0
    belongs_to :user, Takso.User, foreign_key: :user_id
    belongs_to :driver, Takso.User, foreign_key: :driver_id

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  @optional_fields ~w(user_id)a

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:pickup_address, :dropoff_address, :status] ++ @optional_fields)
    |> validate_required([:pickup_address, :dropoff_address, :status])
  end
end
