import EctoEnum

defenum ApplicationStatusEnum, :status, [:pending, :rejected, :accepted]