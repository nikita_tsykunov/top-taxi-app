defmodule Takso.RoleChecker do
    import Ecto.Query
    alias Takso.{User, Repo}


    def is_in_role?(user, role) do
        if user do
            res = Repo.all(from u in User,
                            join: r in assoc(u, :roles),
                            where:  u.id == ^user.id and
                                    r.role == ^role)
            
            
            length(res) > 0
        else
            false
        end
    end
end 
    