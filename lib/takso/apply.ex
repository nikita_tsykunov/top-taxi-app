defmodule Takso.Apply do
  use Ecto.Schema
  import Ecto.Changeset


  schema "apply" do
    field :name, :string
    field :lastName, :string
    field :email, :string
    field :dateOfBirth, :date
    field :licenseNum, :string
    field :licenseCat, :string
    field :additionalInfo, :string
    field :status, ApplicationStatusEnum
    
    belongs_to :user, Takso.User

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:user_id, :name, :lastName, :email, :dateOfBirth, :licenseNum, :licenseCat, :additionalInfo, :status])
    |> validate_required([:user_id, :name, :lastName, :email, :dateOfBirth, :licenseNum, :licenseCat])
    
  end


end
