defmodule Takso.UsersRoles do
    use Ecto.Schema
    import Ecto.Changeset

    @primary_key false
    schema "users_roles" do
        belongs_to :user, Takso.User
        belongs_to :role, Takso.Role
    end

    def changeset(struct, params \\ %{}) do
        struct
        |> cast(params, [:user_id, :role_id])
        |> validate_required([:user_id, :role_id])
    end
end