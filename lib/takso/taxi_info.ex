defmodule Takso.TaxiInfo do
    use Ecto.Schema
    import Ecto.Changeset

    @primary_key false
    @derive {Phoenix.Param, key: :driver_id}
    schema "taxi_info" do
        field :lat, :float, default: 0.0
        field :lng, :float, default: 0.0
        field :rating_score, :float, default: 0.0
        field :rating_qty, :integer, default: 0
        field :car, :string
        field :additional_info, :string
        field :online, :boolean, default: false

        belongs_to :driver, Takso.User, primary_key: true
    end

    def changeset(struct, params \\ %{}) do
        struct
        |> cast(params, [:lat, :lng, :rating_score, :rating_qty, :car, :additional_info, :driver_id, :online])
        |> validate_required([:driver_id])
    end
end
