defmodule Takso.Role do
  use Ecto.Schema
  import Ecto.Changeset

  schema "roles" do
    field :role, :string
    many_to_many :users, Takso.User, join_through: "users_roles"
    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:role])
    |> validate_required([:role])
  end
end
