defmodule TaksoWeb.ApplyController do
    import Ecto.Query
    use TaksoWeb, :controller

	alias Takso.{Repo, Role, User, Apply, TaxiInfo}

    plug :scrub_params, "apply" when action in [:create]

    def new(conn, _params) do
        changeset = Apply.changeset(%Apply{})
        users = Repo.all(from u in User,
                            join: r in assoc(u, :roles),
                            where: r.role == "User")
		render conn, "new.html", changeset: changeset, users: users
    end

    def create(conn, %{"apply" => user_params}) do
        user_params = Map.put user_params, "status", :pending

        changeset = %Apply{} |> Apply.changeset(user_params)
        case Repo.insert(changeset) do

            {:ok, _} ->
                case Takso.RoleChecker.is_in_role?(Guardian.Plug.current_resource(conn), "Operator") do
                    true -> conn
                            |> put_flash(:info, "Application was sent")
                            |> redirect(to: operator_path(conn, :index))
                    false -> conn
                                |> put_flash(:info, "Application was sent, please wait for a response")
                                |> redirect(to: session_path(conn, :show, 1))
                end

            {:error, changeset} ->
                render(conn, "new.html", changeset: changeset)
        end
    end

    def update_status(conn, %{"decision" => decision, "id" => id}) do
        apply = Repo.get(Apply, id) |> Repo.preload(:user)

        changeset = if decision == "true" do
                        params = %{driver_id: apply.user.id}
                        %TaxiInfo{} |> TaxiInfo.changeset(params) |> Repo.insert()

                        user = Repo.get(User, apply.user.id) |> Repo.preload(:roles)
                        role = Repo.get_by(Role, role: "Driver") |> Repo.preload(:users)

                        IO.inspect user
                        user
                        |> Ecto.Changeset.change
                        |> Ecto.Changeset.put_assoc(:roles, [role | user.roles])
                        |> Repo.update

                        Ecto.Changeset.change(apply, status: :accepted)
                    else
                        Ecto.Changeset.change(apply, status: :rejected)
                    end

        case Repo.update(changeset) do
            {:ok, _} ->
            conn
            |> put_flash(:info, "Apply changed")
            |> redirect(to: operator_path(conn, :applications))
        end

    end
end
