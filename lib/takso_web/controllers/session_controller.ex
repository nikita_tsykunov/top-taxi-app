defmodule TaksoWeb.SessionController do

	import Plug.Conn

	alias Takso.{Repo, User}

	use TaksoWeb, :controller
		plug :scrub_params, "session" when action in ~w(create)a

	def new(conn, _) do
		render conn, "new.html"
	end

	def show(conn, %{"id" => id}) do
        user = Repo.get!(User, id)
        render(conn, "show.html", user: user)
    end

	def create(conn, %{"session" => %{"phone_number" => phone_number, "password" => password, "is_driver" => is_driver}}) do

		case TaksoWeb.Auth.login_by_phone_and_pass(conn, phone_number, password, is_driver) do
			{:ok, conn} ->
				if is_driver == "false" do
					case Takso.RoleChecker.is_in_role?(Guardian.Plug.current_resource(conn), "Operator") do
						true -> conn 
								|> put_flash(:info, "You`re now logged in!")
								|> Plug.Conn.put_session(:login, "Operator")
								|> redirect(to: operator_path(conn, :index))
						false -> conn 
								 |> put_flash(:info, "You`re now logged in!")
								 |> Plug.Conn.put_session(:login, "User")
								 |> redirect(to: session_path(conn, :show, 1))
					end

				else
					conn 
					|> put_flash(:info, "You`re now logged in!")
					|> Plug.Conn.put_session(:login, "Driver")
					|> redirect(to: order_path(conn, :index))
				end
			{:error, _reason, conn} ->
				conn
				|> put_flash(:error, "Invalid phone number/password combination")
				|> render("new.html")
		end
	end

	def delete(conn, _) do
		conn
		|> TaksoWeb.Auth.logout
		|> put_flash(:info, "See you later!")
		|> redirect(to: page_path(conn, :index))
	end

end
