defmodule TaksoWeb.UserController do
    use TaksoWeb, :controller

    alias Takso.{Repo, User, Role}

    plug :scrub_params, "user" when action in [:create]

    def show(conn, %{"id" => id}) do
        user = Repo.get!(User, id)
        render(conn, "show.html", user: user)
    end

    def new(conn, _params) do
        changeset = User.changeset(%User{})
        render conn, "new.html", changeset: changeset
    end

    def create(conn, %{"user" => user_params}) do
        changeset = %User{} |> User.registration_changeset(user_params)
        case Repo.insert(changeset) do
            {:ok, user} ->
                role = Repo.get_by(Role, role: "User")

                user
                |> Repo.preload(:roles)
                |> Ecto.Changeset.change
                |> Ecto.Changeset.put_assoc(:roles, [role])
                |> Repo.update

                conn
                |> TaksoWeb.Auth.login(user)
                |> put_flash(:info, "#{user.name} created!")
                |> redirect(to: session_path(conn, :show, 1))

            {:error, changeset} ->
                render(conn, "new.html", changeset: changeset)
        end
    end
end
