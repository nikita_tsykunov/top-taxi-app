import Ecto.Query

defmodule TaksoWeb.OrderController do
    use TaksoWeb, :controller

    alias Takso.{Repo, Order, TaxiInfo, User}

    def index(conn, _params) do
      current_user = Guardian.Plug.current_resource(conn)
      query = from order in Order, where: order.status == "pending" and order.driver_id == ^current_user.id
      orders = Repo.all(query)
      render(conn, "index.html", orders: orders)
    end

    def user_show(conn, %{"id" => id}) do
      order = Repo.get(Order, id)
      driver = Repo.get(User, order.driver_id)
      info = Repo.get(TaxiInfo, driver.id)
      render(conn, "user_show.html", order: order, driver: driver, taxi_info: info)
    end

    def accept(conn, _params) do
      user_id = Guardian.Plug.current_resource(conn).id
      orders = Repo.all(from o in Order, join: u in assoc(o, :user), where: u.id == ^user_id)
      |> Enum.sort_by(& &1.id, & >=/2)
      IO.inspect hd(orders)
      order = hd(orders) |> assign_driver
      case order do
        nil -> conn
        |> put_flash(:error, "No drivers available! Try again later")
        |> redirect(to: session_path(conn, :show, 1))
        _ -> redirect(conn, to: order_path(conn, :user_show, order.id))
      end
    end

    def assign_driver(order) do
      drivers = Repo.all(from u in User, join: r in assoc(u, :roles), where: r.role == "Driver")
      distances = Enum.map(drivers, fn d ->
        info = Repo.get_by(TaxiInfo, driver_id: d.id, online: true)
        case info do
          nil -> %{driver: d, distance: -1}
          _ -> %{driver: d, distance: Distance.GreatCircle.distance({order.pickup_lng, order.pickup_lat}, {info.lng, info.lat})}
        end
      end) |> Enum.filter(fn d -> d.distance > -1 end)
      case distances |> Enum.empty? do
        true -> nil
        false ->
        distances = distances |> Enum.sort_by(& &1.distance)
        closest_driver = Enum.at(distances, order.declines, :none).driver
        case closest_driver do
          :none -> nil
          _ ->
          case Ecto.Changeset.change(order, driver_id: closest_driver.id) |> Repo.update do
            {:ok, order} -> order
            _ -> nil
          end
        end
      end
    end

    def cancel(conn, _params) do
      user_id = Guardian.Plug.current_resource(conn).id
      orders = Repo.all(from o in Order, join: u in assoc(o, :user), where: u.id == ^user_id)
      |> Enum.sort_by(& &1.id, & >=/2)
      order = hd(orders)
      Ecto.Changeset.change(order, status: "cancelled") |> Repo.update
      conn
      |> put_flash(:info, "You have cancelled your order!")
      |> redirect(to: session_path(conn, :show, 1))
    end

    def action_update(conn, %{"action" => action, "id" => id}) do
      case action do
        "accept" ->   order = Repo.get!(Order, id)
                      changeset = Ecto.Changeset.change(order, status: "active")
                      case Repo.update(changeset) do
                        {:ok, changeset} ->
                          conn
                          |> redirect(to: order_path(conn, :active, id: changeset.id))
                      end
        "reject" ->   order = Repo.get!(Order, id)
                      case assign_driver(order) do
                        nil -> Ecto.Changeset.change(order, driver_id: nil) |> Repo.update
                      end
                      conn
                      |> put_flash(:info, "Order was rejected!")
                      |> redirect(to: order_path(conn, :index))
      end
    end

    def active(conn, %{"id" => id}) do
      order = Repo.get!(Order, id)
      render(conn, "active_order.html", order: order)
    end

    def finish(conn, %{"id" => id, "dropoff_lng" => dropoff_lng, "dropoff_lat" => dropoff_lat}) do
      {dropoff_lng, _} = Float.parse(dropoff_lng)
      {dropoff_lat, _} = Float.parse(dropoff_lat)
      current_user = Guardian.Plug.current_resource(conn)
      info = Repo.get_by(TaxiInfo, driver_id: current_user.id)
      info_changeset = Ecto.Changeset.change(info, lat: dropoff_lat, lng: dropoff_lng)
      Repo.update(info_changeset)
      order = Repo.get!(Order, id)
      changeset = Ecto.Changeset.change(order, status: "completed")
      case Repo.update(changeset) do
        {:ok, _}       ->
          conn
          |> redirect(to: order_path(conn, :index))
      end
    end

    def history(conn, _params) do
      current_user = Guardian.Plug.current_resource(conn)
      query = from order in Order, where: order.driver_id == ^current_user.id and order.status == "completed"
      orders = Repo.all(query)
      render(conn, "history.html", orders: orders)
    end

    def create(conn, %{"user_id" => user_id,
                       "pickup_lat" => pickup_lat,
                       "pickup_lng" => pickup_lng,
                       "pickup_address" => pickup_address,
                       "dropoff_lat" => dropoff_lat,
                       "dropoff_lng" => dropoff_lng,
                       "dropoff_address" => dropoff_address}) do
      {user_id, _} = Integer.parse(user_id)
      case Repo.insert %Order{status: "pending", user_id: user_id,
        pickup_lat: pickup_lat,
        pickup_lng: pickup_lng,
        pickup_address: pickup_address,
        dropoff_lat: dropoff_lat,
        dropoff_lng: dropoff_lng,
        dropoff_address: dropoff_address} do
            {:ok, _order} -> put_status(conn, 201) |> json(%{msg: "Order was booked!"})
            _ -> put_status(conn, 500) |> json(%{msg: "Something went wrong!"})
      end
    end
end
