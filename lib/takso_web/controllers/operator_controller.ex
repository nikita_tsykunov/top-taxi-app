defmodule TaksoWeb.OperatorController do
    import Ecto.Query
    use TaksoWeb, :controller
    alias Takso.{Repo, User, Order, Apply, TaxiInfo}

    def index(conn, _params) do
        users = Repo.all(from u in User,
                            join: r in assoc(u, :roles),
                            where: r.role == "User")
        render(conn, "index.html", users: users)
    end

    def edit(conn, %{"id" => id}) do
        info = Repo.get(TaxiInfo, id)
        changeset = TaxiInfo.changeset(info)
        applies = Repo.all(from a in Apply,
                         join: u in assoc(a, :user),
                         on: u.id == a.user_id,
                         where: u.id == ^id and
                                a.status == ^:accepted)
        render(conn, "edit.html", info: info, applies: applies, changeset: changeset)
    end

    def update(conn,  %{"id" => id, "taxi_info" => info_params}) do
        info = Repo.get(TaxiInfo, id)
        changeset = info |> TaxiInfo.changeset(info_params)
        
        drivers = Repo.all(from u in User,
                            join: r in assoc(u, :roles),
                            where: r.role == "Driver")

        applies = Repo.all(from a in Apply,
                            join: u in assoc(a, :user),
                            on: u.id == a.user_id,
                            where: u.id == ^id and
                                   a.status == ^:accepted)

        case Repo.update(changeset) do 
            {:ok, _} ->
                conn
                |> put_flash(:info, "Updated successfully!")
                |> redirect(to: operator_path(conn, :drivers, drivers))
            {:error, changeset} ->
                render conn, "edit.html", changeset: changeset, applies: applies, info: info
        end
    end

    def drivers(conn, _params) do
        drivers = Repo.all(from u in User,
                            join: r in assoc(u, :roles),
                            where: r.role == "Driver")
        
        render(conn, "drivers.html", drivers: drivers)                            
    end

    def orders(conn, _params) do        
        query = from order in Order,
                join: user in assoc(order, :user),
                join: driver in assoc(order, :driver),
                order_by: [desc: order.id],
                select: %{user_name: user.name,
                          from: order.pickup_address,
                          to: order.dropoff_address,
                          driver_name: driver.name,
                          status: order.status}

        orders = Repo.all(query)
        render(conn, "orders.html", orders: orders)                            
    end

    def applications(conn, _params) do
        pendings  = Repo.all(from a in Apply,
                              where: a.status == ^:pending,
                              order_by: [desc: a.inserted_at])
        
        rejected = Repo.all(from a in Apply,
                              where: a.status == ^:rejected,
                              order_by: [desc: a.inserted_at])
                              
        
        accepted = Repo.all(from a in Apply,
                              where: a.status == ^:accepted,
                              order_by: [desc: a.inserted_at])

        render(conn, "applications.html", pendings: pendings, rejected: rejected, accepted: accepted)
    end
end