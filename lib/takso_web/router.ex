defmodule TaksoWeb.Router do
  use TaksoWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

	pipeline :with_session do
	  plug Guardian.Plug.VerifySession
	  plug Guardian.Plug.LoadResource
	  plug TaksoWeb.CurrentUser
  end

  pipeline :taxi_required do
    plug TaksoWeb.CheckTaxi
  end

  pipeline :operator_required do
    plug TaksoWeb.CheckOperator
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", TaksoWeb do
    pipe_through [:browser, :with_session]

    get "/", PageController, :index
    resources "/users", UserController, only: [:show, :new, :create]
    resources "/sessions", SessionController, only: [:new, :create, :delete, :show]

    scope "/orders" do
      get "/accept", OrderController, :accept
      get "/cancel", OrderController, :cancel
      get "/:id", OrderController, :user_show
    end

    scope "/apply" do
      resources "/", ApplyController, only: [:new, :create, :delete, :show]
      post "/update", ApplyController, :update_status
    end

    scope "/taxi" do
      pipe_through [:taxi_required]

      resources "/orders", OrderController, only: [:index, :update]
      get "/history", OrderController, :history
      post "/action", OrderController, :action_update
      get "/active", OrderController, :active
      post "/finish", OrderController, :finish
      resources "/", TaxiController, only: [:index, :new]

    end

    scope "/operator" do
      pipe_through [:operator_required]

      resources "/", OperatorController, only: [:index, :edit, :update]
      get "/drivers", OperatorController, :drivers
      get "/orders", OperatorController, :orders
      get "/applications", OperatorController, :applications
    end
  end

  scope "/client", TaksoWeb do
    pipe_through [:api]

    post "/orders", OrderController, :create
  end
end
