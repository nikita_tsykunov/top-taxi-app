defmodule TaksoWeb.CheckOperator do
    import Phoenix.Controller
    import Plug.Conn

    def init(opts), do: opts

    def call(conn, _opts) do
        current_user = Guardian.Plug.current_resource(conn)
        
        if Takso.RoleChecker.is_in_role?(current_user, "Operator") do
            conn
        else
            conn
            |> put_status(:not_found)
            |> render(TaksoWeb.ErrorView, "404.html")
            |> halt
        end
    end
end