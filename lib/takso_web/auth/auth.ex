defmodule TaksoWeb.Auth do

    alias Takso.{User, Repo}

    def login(conn, user) do
      conn
      |> Guardian.Plug.sign_in(user)
    end

    def login_by_phone_and_pass(conn, phone_number, given_pass, is_driver) do
        user = Repo.get_by(User, phone_number: phone_number) |> Repo.preload(:roles)
        
        condition = if user do
                        if is_driver == "true" do
                            (given_pass == user.password) && Enum.any?(user.roles, &(&1.role == "Driver"))
                        else
                            given_pass == user.password
                        end
                    else
                        false
                    end
        
        cond do
            user && condition -> {:ok, login(conn, user)}
            user -> {:error, :unauthorized, conn}
            true -> {:error, :not_found, conn}
        end
    end

    def logout(conn) do
      Guardian.Plug.sign_out(conn)
    end
end
