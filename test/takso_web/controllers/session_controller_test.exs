defmodule TaksoWeb.SessionControllerTest do
  use TaksoWeb.ConnCase

  alias Takso.{Repo, User, Role}
  @derive {Phoenix.Param, key: :id}

@valid_attrs %{ password: "password", phone_number: "12345678", "is_driver": false}
test "create", %{conn: conn} do
	{:ok, user} = Repo.insert(%User{name: "andrei", phone_number: "12345678",
                                    password: "password"})
	User.changeset(%User{}, @valid_attrs)
     response = conn
      |> post(session_path(conn, :create, session: @valid_attrs))
   
    user = Repo.get_by!(User, phone_number: "12345678")
    assert response.params ==  %{"session" => %{"password" => user.password, "phone_number" => user.phone_number, "is_driver" => "false"}}

  end

  test "show", %{conn: conn} do
  {:ok, user} = Repo.insert(%User{name: "andrei", phone_number: "12345678",
                                    password: "password"})

	user2 = Repo.get_by!(User, phone_number: "12345678")
    ##response = conn
     ## |> get(session_path(conn, :show, user))

  end

  test "new", %{conn: conn} do
    {:ok, user} = Repo.insert(%User{name: "andrei", phone_number: "12345678",
                                    password: "password"})
    response = conn
      |> get(session_path(conn, :new, changeset: user))

    assert response.assigns.current_user == nil
  end

  
end
