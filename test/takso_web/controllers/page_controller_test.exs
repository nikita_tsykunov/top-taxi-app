defmodule TaksoWeb.PageControllerTest do
  use TaksoWeb.ConnCase

  import Phoenix.View

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "Strelsau Taxi"
  end

  test "renders 404.html" do
    assert render_to_string(TaksoWeb.ErrorView, "404.html", []) ==
           "Not Found"
  end

  test "renders 500.html" do
    assert render_to_string(TaksoWeb.ErrorView, "500.html", []) ==
           "Internal Server Error"
  end
end
