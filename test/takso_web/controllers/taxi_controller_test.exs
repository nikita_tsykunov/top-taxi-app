defmodule TaksoWeb.TaxiControllerTest do
  use TaksoWeb.ConnCase

  import Phoenix.View

  test "index", %{conn: conn} do
    response =
      conn
      |> get(taxi_path(conn, :index))

	  assert response !== nil
  end

  test "new", %{conn: conn} do
    response =
      conn
      |> get(taxi_path(conn, :new))

	  assert response.resp_body == "Not Found"
  end



  
end