defmodule TaksoWeb.OrderControllerTest do
  use TaksoWeb.ConnCase

  alias Takso.{Repo, User, Role}
  @derive {Phoenix.Param, key: :id}

test "history", %{conn: conn} do
	
     response = conn
      |> get(order_path(conn, :history))
    
    assert response !== nil
  end

  test "index", %{conn: conn} do
	{:ok, user} = Repo.insert(%User{name: "andrei", phone_number: "12345678",
                                    password: "password"})
     response = conn
      |> get(order_path(conn, :index))
    
    assert response.assigns.current_user == nil
  end

@valid_attrs %{action: "accept", id: "1"}
test "action_update", %{conn: conn} do
	
     response = conn
      |> post(order_path(conn, :action_update, @valid_attrs))
    
    assert response !== nil
  end

  @valid_attrs %{action: "reject", id: "1"}
test "action_update2", %{conn: conn} do
	
     response = conn
      |> post(order_path(conn, :action_update, @valid_attrs))
    
    assert response !== nil
  end

  
end
