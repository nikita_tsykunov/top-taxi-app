defmodule TaksoWeb.OperatorControllerTest do
  use TaksoWeb.ConnCase

  alias Takso.{Repo, User, Role}
  @derive {Phoenix.Param, key: :id}

test "index", %{conn: conn} do
	
     response = conn
      |> get(operator_path(conn, :index))
    
    assert response !== nil
  end

  test "drivers", %{conn: conn} do
	
     response = conn
      |> get(operator_path(conn, :drivers))
    
    assert response !== nil
  end

  test "orders", %{conn: conn} do
	
     response = conn
      |> get(operator_path(conn, :orders))
    
    assert response !== nil
  end

  test "applications", %{conn: conn} do
	
     response = conn
      |> get(operator_path(conn, :applications))
    
    assert response !== nil
  end

  
end
