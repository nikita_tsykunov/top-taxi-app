defmodule TaksoWeb.UserControllerTest do
  use TaksoWeb.ConnCase

  alias Takso.{Repo, User, Role}
  @derive {Phoenix.Param, key: :id}

  test "Responds with user info if the user is found", %{conn: conn} do
    {:ok, user} = Repo.insert(%User{name: "andrei", phone_number: "12345678",
                                    password: "password"})
    response = conn
      |> get(user_path(conn, :show, user.id))

    assert response.assigns.user.name == user.name
    assert response.assigns.user.phone_number == user.phone_number
  end

  test "user controller new", %{conn: conn} do
    {:ok, user} = Repo.insert(%User{name: "andrei", phone_number: "12345678",
                                    password: "password"})
    response = conn
      |> get(user_path(conn, :new, changeset: user))

    assert response.assigns.current_user == nil
  end

  @valid_attrs %{name: "andrei", password: "password", phone_number: "12345678"}
  test "create", %{conn: conn} do
    changeset = User.changeset(%User{}, @valid_attrs)
    response = conn
      |> post(user_path(conn, :create, user: @valid_attrs))
    user = Repo.get_by!(User, phone_number: "12345678")
    
    assert response.params ==  %{"user" => %{"name" => user.name,
      "password" => user.password, "phone_number" => user.phone_number}}
  end
end
