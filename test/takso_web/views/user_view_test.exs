defmodule TaksoWeb.UserViewTest do
  use TaksoWeb.ConnCase, async: true
  import Phoenix.View
  alias Takso.{User, Repo}

  test "renders show.html", %{conn: conn} do
    Repo.insert(%User{name: "andrei", phone_number: "12345678",
                                    password: "password"})
    user = Repo.get_by!(User, phone_number: "12345678")
    content = render_to_string(TaksoWeb.UserView, "show.html", conn: conn, user: user)
    assert String.contains?(content, user.name)
  end

  @valid_attrs %{name: "andrei", password: "password", phone_number: "12345678"}
  test "renders new.html", %{conn: conn} do
    changeset = User.changeset(%User{}, @valid_attrs)
    content = render_to_string(TaksoWeb.UserView, "new.html", conn: conn, changeset: changeset)
    assert String.contains?(content, "Create User")
  end
end
