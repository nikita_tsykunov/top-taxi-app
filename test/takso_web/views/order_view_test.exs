import Ecto.Query
import Phoenix.View

defmodule TaksoWeb.OrderViewTest do
  use TaksoWeb.ConnCase, async: true
  alias Takso.{Order, Repo}

  test "renders index.html", %{conn: conn} do
    order = Repo.insert!(%Order{pickup_address: "Raatuse 22", dropoff_address: "X", status: "pending"})
    query = from order in Order, where: order.status == "pending"
    orders = Repo.all(query)
    content = render_to_string(TaksoWeb.OrderView, "index.html", conn: conn, orders: orders)
    assert String.contains?(content, order.pickup_address)
  end

  test "renders new.html", %{conn: conn} do
    order = Repo.insert!(%Order{pickup_address: "A", dropoff_address: "B", status: "completed"})
    query = from order in Order, where: order.status == "completed"
    orders = Repo.all(query)
    content = render_to_string(TaksoWeb.OrderView, "history.html", conn: conn,  orders: orders)
    assert String.contains?(content, order.dropoff_address)
  end

  test "renders active_order.html", %{conn: conn} do
    order = Repo.insert!(%Order{pickup_address: "Raatuse 22", dropoff_address: "X", status: "active"})
    content = render_to_string(TaksoWeb.OrderView, "active_order.html", conn: conn,  order: order)
    assert String.contains?(content, order.pickup_address)
  end
end
