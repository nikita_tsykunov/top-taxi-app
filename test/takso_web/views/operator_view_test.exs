import Ecto.Query
import Phoenix.View

defmodule TaksoWeb.OperatorViewTest do
  use TaksoWeb.ConnCase, async: true
  alias Takso.{Operator, User, Repo, Role}

  test "renders index.html", %{conn: conn} do
    users = Repo.all(from u in User,
                        join: r in assoc(u, :roles),
                        on: u.id == r.id and
                            r.role == "User")

    content = render_to_string(TaksoWeb.OperatorView, "index.html", conn: conn, users: users)
    assert String.contains?(content, "Applications")
  end
end
