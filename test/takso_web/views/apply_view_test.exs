import Ecto.Query
import Phoenix.View

defmodule TaksoWeb.ApplyViewTest do
  use TaksoWeb.ConnCase, async: true
  alias Takso.{User, Repo}

  @valid_attrs %{name: "andrei", password: "password", phone_number: "12345678"}
  test "renders new.html", %{conn: conn} do
    {:ok, user} = Repo.insert(%User{name: "andrei", phone_number: "12345678",
                                    password: "password"})
    changeset = User.changeset(%User{}, @valid_attrs)
    content = render_to_string(TaksoWeb.ApplyView, "new.html", conn: conn,
      changeset: changeset, current_user: user)
    assert String.contains?(content, "Licence Category")
  end
end
