defmodule TaksoWeb.SessionViewTest do
  use TaksoWeb.ConnCase, async: true
  import Phoenix.View
  alias Takso.{User, Repo}

  test "renders show.html", %{conn: conn} do
    Repo.insert(%User{name: "andrei", phone_number: "12345678",
                                    password: "password"})
    user = Repo.get_by!(User, phone_number: "12345678")
    content = render_to_string(TaksoWeb.SessionView, "show.html", conn: conn, current_user: user)
    assert String.contains?(content, "Information about the ride:")
  end

  test "renders new.html", %{conn: conn} do
    content = render_to_string(TaksoWeb.SessionView, "new.html", conn: conn)
    assert String.contains?(content, "Sign in")
  end
end
